import webapp2
import jinja2
import os

jinja_dir = jinja2.Environment(
  loader = jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'htmls')))
class MainPage(webapp2.RequestHandler):
  def get(self):
      
      template = jinja_dir.get_template('index.html')
      self.response.write(template.render({}))
      
      '''self.response.headers['Content-Type'] = 'text/plain'
      self.response.write('Hello, webapp2 World!')'''

app = webapp2.WSGIApplication([('/', MainPage)],
                              debug=True)
